FROM jjanzic/docker-python3-opencv:opencv-3.4.0
RUN apt-get update

## Python installation ##
RUN apt-get install -y python3-pip gcc build-essential git wget swig3.0 \
 libhdf5-dev
RUN pip3 install tensorflow cython jupyter matplotlib

## Darkflow ##
RUN cd "/" && \
	git clone https://github.com/thtrieu/darkflow.git &&\
	cd darkflow && \
	pip3 install . && \
	cd "/" && \
	rm -rf darkflow

# Add data
ADD ./docker-entrypoint.sh ./docker-entrypoint.sh
RUN chmod +x docker-entrypoint.sh
RUN mkdir -p /notebooks

# change working dir to /notebooks
WORKDIR /notebooks
ADD ./data /notebooks
RUN pip3 install --upgrade pip
RUN pip3 install -r requirements.txt

# Install
ENTRYPOINT ["/docker-entrypoint.sh"]
EXPOSE 8888 5000
